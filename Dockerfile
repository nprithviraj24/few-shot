FROM nvidia/cuda:10.2-cudnn8-devel-ubuntu18.04

RUN echo "export LD_LIBRARY_PATH=/usr/local/cuda-10.2/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}" >> /root/.bashrc
RUN echo "export PATH=/usr/local/cuda-10.2/bin${PATH:+:${PATH}}" >> /root/.bashrc

RUN apt-get update && apt-get upgrade -y 
# set the locale to en_US.UTF-8
RUN apt-get install -y locales
ENV DEBIAN_FRONTEND noninteractive
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen en_US.UTF-8 \
    && dpkg-reconfigure locales \
    && /usr/sbin/update-locale LANG=en_US.UTF-8
ENV LC_ALL en_US.UTF-8

RUN apt-get install -y python3-pip python3-dev

RUN useradd --create-home --shell /bin/bash ubuntu 
USER ubuntu 
WORKDIR /home/ubuntu

RUN pip3 install -U setuptools 
RUN python3 -m pip install --upgrade pip && python3 -m pip install --upgrade Pillow 
RUN pip3 install numpy torch torchvision
RUN pip3 install tensorboard ipykernel jupyter jupyterlab matplotlib
RUN pip3 install cython==0.28.5
RUN pip3 install scipy==0.19.1
RUN pip3 install sklearn pandas seaborn

RUN pip3 install --upgrade pip
RUN pip3 install opencv-python

EXPOSE 8888
# ENTRYPOINT [ "jupyter" "notebook" "--ip" "0.0.0.0" "--no-browser" ]
# CMD [ "jupyter notebook --ip 0.0.0.0 --no-browser" ]