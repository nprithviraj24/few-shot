
from triplet_loss.utils import in_notebook
from triplet_loss.dataset import CattleDataset #, load_image
from torch.utils import data
import torch
import numpy as np
# from torch.optim.lr_scheduler import StepLR

if in_notebook():
    from tqdm import tqdm_notebook as tqdm
else:
    from tqdm import tqdm

def validate(model, validation_dataloader, device, loss_func): 
    model.eval()
    batch_losses = []

    with torch.no_grad():
        for sample in validation_dataloader:
            for key in ['anchor','positive','negative']:
                sample[key] = sample[key].to(device)

        anchor_embed = model(sample['anchor'])
        positive_embed = model(sample['positive'])
        negative_embed = model(sample['negative'])
        loss = loss_func(anchor_embed, positive_embed, negative_embed) 

        batch_losses.append(loss.item())
    return batch_losses

def train_epoch(model, train_dataloader, optimizer, device, loss_func):
    """Train one epoch"""
    batch_losses = []

    for sample in tqdm(train_dataloader):
        model.train()
        optimizer.zero_grad()

        for key in ['anchor','positive','negative']:
            sample[key] = sample[key].to(device)

        anchor_embed = model(sample['anchor'])
        positive_embed = model(sample['positive'])
        negative_embed = model(sample['negative'])
        loss = loss_func(anchor_embed, positive_embed, negative_embed)  
        loss.backward()
        
        optimizer.step()

#         lr_scheduler.step()
        batch_losses.append(loss.item())
    return batch_losses

def train(model, train_dataloader, validation_dataloader, optimizer, device, loss_func, EPOCHS, lr_scheduler=None,  tb_writer=None):

    train_losses = []
    validation_losses = []
    # lr_scheduler = StepLR(optimizer, step_size=10, gamma=0.4)
    for epoch in range(1, EPOCHS+1):
        batch_losses = train_epoch(model, train_dataloader, optimizer, device, loss_func)

        if lr_scheduler: lr_scheduler.step()

        mean_loss = np.mean(batch_losses)
    
        if tb_writer: tb_writer.add_scalar('Loss/train', mean_loss, epoch)        
        train_losses.append(mean_loss)
        val_loss = validate(model, validation_dataloader, device, loss_func)
        val_loss_mean = np.mean(val_loss)
        validation_losses.append(val_loss_mean)
        if tb_writer: tb_writer.add_scalar('Loss/validation', val_loss_mean, epoch)
        print('====Epoch {}. Train loss: {}. Val loss: {}'.format(epoch,  mean_loss,  val_loss_mean))
    return model

def embedding_per_image(model, embed_dataloader, device):
    embeddings_dict = {}

    model.eval()
    with torch.no_grad():
        for sample in tqdm(embed_dataloader):
            anchors = sample['anchor'].to(device)
            embeds = model(anchors)
    
            for image_name, embed in zip(sample['name'], embeds):
                embeddings_dict[image_name] = embed.cpu().numpy()
    return embeddings_dict

# def main():
#     td = CattleDataset(train_df, augment=True)
#     train_dataloader = data.DataLoader(td, batch_size=32, shuffle=True, num_workers = 4, drop_last=False)