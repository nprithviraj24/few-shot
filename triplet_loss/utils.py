from os.path import isfile
from matplotlib import pyplot as plt

def in_notebook():
    try:
        from IPython import get_ipython
        if 'IPKernelApp' not in get_ipython().config:  # pragma: no cover
            return False
    except (ImportError, AttributeError) :
        return False
    return True

def find_top_k(sims, k, train_df):
    top_sims = sims.argsort()[::-1]
    top_klasses = set(['new_whale'])
    for sim in top_sims:
        klass = train_df.iloc[sim]['Id']
        top_klasses.add(klass)
        if len(top_klasses) == k:
            break
    return ' '.join(top_klasses)


def expand_path(p, TRAIN, TEST):
    '''Takes image name and returns full path'''
    if isfile(TRAIN + p):
        return TRAIN + p
    if isfile(TEST + p):
        return TEST + p
    return p

def tensor2numpy(d):
  return d.permute(1, 2, 0).cpu().numpy()

def plot_sample(sample):
    """sample is a item from CattleDataset"""
    # create figure
    fig = plt.figure(figsize=(15, 10))
    
    # setting values to rows and column variables
    rows = 1
    columns = 3
    
    # Adds a subplot at the 1st position
    fig.add_subplot(rows, columns, 1)
    
    # showing image
    plt.imshow(tensor2numpy(sample['anchor']))
    plt.axis('off')
    plt.title("Anchor")
    
    # Adds a subplot at the 2nd position
    fig.add_subplot(rows, columns, 2)
    
    # showing image
    plt.imshow(tensor2numpy(sample['positive']))
    plt.axis('off')
    plt.title("Positive")
    
    # Adds a subplot at the 3rd position
    fig.add_subplot(rows, columns, 3)
    
    # showing image
    plt.imshow(tensor2numpy(sample['negative']))
    plt.axis('off')
    plt.title("Negative")
    
    # Adds a subplot at the 4th position
    # fig.add_subplot(rows, columns, 4)
    plt.show()