import torch.nn as nn
from torchvision import models

EMBEDDINGS_SIZE = 512 #1000
### Pretrained Resnet18/34/50  

class ResNet34(nn.Module):
    '''
    Last fully connected layer changed to ouput EMBEDDINGS_SIZE-dim vector.
    '''
    
    def __init__(self, embedSize=EMBEDDINGS_SIZE, freeze=True):
        super(ResNet34, self).__init__()
        self.model = models.resnet34(pretrained=True)
        if freeze:
            for param in self.model.parameters():
                param.requires_grad = False
        self.model.fc = nn.Linear(in_features=512, out_features=embedSize, bias=True)

    def forward(self, image):
        features = self.model(image)
        return features

class ResNet18(nn.Module):
    '''
    Last fully connected layer changed to ouput EMBEDDINGS_SIZE-dim vector.
    '''

    def __init__(self, embedSize=EMBEDDINGS_SIZE, freeze=True):
        super(ResNet18, self).__init__()
        self.model = models.resnet18(pretrained=True)
        if freeze:
            for param in self.model.parameters():
                param.requires_grad = False
        self.model.fc = nn.Linear(in_features=512, out_features=embedSize, bias=True)

    def forward(self, image):
        features = self.model(image)
        return features

class ResNet50(nn.Module):
    '''
    Last fully connected layer changed to ouput EMBEDDINGS_SIZE-dim vector.
    '''

    def __init__(self, embedSize=EMBEDDINGS_SIZE, freeze=True):
        super(ResNet50, self).__init__()
        self.model = models.resnet50(pretrained=True)
        if freeze:
            for param in self.model.parameters():
                param.requires_grad = False

        #self.model.fc = nn.Linear(in_features=2048, out_features=1000, bias=True)
        self.model.fc = nn.Linear(in_features=2048, out_features=embedSize, bias=True)

    def forward(self, image):
        features = self.model(image)
        return features
