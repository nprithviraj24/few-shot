```bash
docker run -it --rm --gpus all -v $(pwd)/few-shot:/tmp -p 8888:8888 torch-cuda102 jupyter notebook --ip 0.0.0.0 --allow-root
```