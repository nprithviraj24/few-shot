#%%
from tqdm import tqdm
import numpy as np
import torch
import os
from protonet.model import ProtoNet
from protonet.batchsampler import PrototypicalBatchSampler
# from protonet.omniglot_dataset import OmniglotDataset
from protonet.cattle_dataset import CattleDataset
from protonet.loss import prototypical_loss as loss_fn
#%%

def init_seed(opt):
    '''
    Disable cudnn to maximize reproducibility
    '''
    torch.cuda.cudnn_enabled = False
    np.random.seed(opt.manual_seed)
    torch.manual_seed(opt.manual_seed)
    torch.cuda.manual_seed(opt.manual_seed)


def init_dataset(opt, df, mode):
    dataset = CattleDataset(df, mode=mode)
    n_classes = len(np.unique(dataset.y))
    if n_classes < opt.classes_per_it_tr or n_classes < opt.classes_per_it_val:
        raise(Exception('There are not enough classes in the dataset in order ' +
                        'to satisfy the chosen classes_per_it. Decrease the ' +
                        'classes_per_it_{tr/val} option and try again.'))
    return dataset


def init_sampler(opt, labels, mode):
    if 'train' in mode:
        classes_per_it = opt.classes_per_it_tr
        num_samples = opt.num_support_tr + opt.num_query_tr
    else:
        classes_per_it = opt.classes_per_it_val
        num_samples = opt.num_support_val + opt.num_query_val

    return PrototypicalBatchSampler(labels=labels,
                                    classes_per_it=classes_per_it,
                                    num_samples=num_samples,
                                    iterations=opt.iterations)


def init_dataloader(opt, df, mode):
    dataset = init_dataset(opt, df, mode)
    sampler = init_sampler(opt, dataset.y, mode)
    dataloader = torch.utils.data.DataLoader(dataset, batch_sampler=sampler)
    return dataloader


def init_protonet(opt):
    '''
    Initialize the ProtoNet
    '''
    device = 'cuda:0' if torch.cuda.is_available() and opt.cuda else 'cpu'
    model = ProtoNet().to(device)
    return model


def init_optim(opt, model):
    '''
    Initialize optimizer
    '''
    return torch.optim.Adam(params=model.parameters(),
                            lr=opt.learning_rate)


def init_lr_scheduler(opt, optim):
    '''
    Initialize the learning rate scheduler
    '''
    return torch.optim.lr_scheduler.StepLR(optimizer=optim,
                                           gamma=opt.lr_scheduler_gamma,
                                           step_size=opt.lr_scheduler_step)


def save_list_to_file(path, thelist):
    with open(path, 'w') as f:
        for item in thelist:
            f.write("%s\n" % item)


def train(opt, tr_dataloader, model, optim, lr_scheduler, val_dataloader=None):
    '''
    Train the model with the prototypical learning algorithm
    '''

    device = 'cuda:0' if torch.cuda.is_available() and opt.cuda else 'cpu'

    if val_dataloader is None:
        best_state = None
    train_loss = []
    train_acc = []
    val_loss = []
    val_acc = []
    best_acc = 0

    best_model_path = os.path.join(opt.experiment_root, 'best_model.pth')
    last_model_path = os.path.join(opt.experiment_root, 'last_model.pth')

    for epoch in range(opt.epochs):
        print('=== Epoch: {} ==='.format(epoch))
        tr_iter = iter(tr_dataloader)
        model.train()
        for batch in tqdm(tr_iter):
            optim.zero_grad()
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)
            loss, acc = loss_fn(model_output, target=y,
                                n_support=opt.num_support_tr)
            loss.backward()
            optim.step()
            train_loss.append(loss.item())
            train_acc.append(acc.item())
        avg_loss = np.mean(train_loss[-opt.iterations:])
        avg_acc = np.mean(train_acc[-opt.iterations:])
        print('Avg Train Loss: {}, Avg Train Acc: {}'.format(avg_loss, avg_acc))
        lr_scheduler.step()
        if val_dataloader is None:
            continue
        val_iter = iter(val_dataloader)
        model.eval()
        for batch in val_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)
            loss, acc = loss_fn(model_output, target=y,
                                n_support=opt.num_support_val)
            val_loss.append(loss.item())
            val_acc.append(acc.item())
        avg_loss = np.mean(val_loss[-opt.iterations:])
        avg_acc = np.mean(val_acc[-opt.iterations:])
        postfix = ' (Best)' if avg_acc >= best_acc else ' (Best: {})'.format(
            best_acc)
        print('Avg Val Loss: {}, Avg Val Acc: {}{}'.format(
            avg_loss, avg_acc, postfix))
        if avg_acc >= best_acc:
            torch.save(model.state_dict(), best_model_path)
            best_acc = avg_acc
            best_state = model.state_dict()

    torch.save(model.state_dict(), last_model_path)

    for name in ['train_loss', 'train_acc', 'val_loss', 'val_acc']:
        save_list_to_file(os.path.join(opt.experiment_root,
                                       name + '.txt'), locals()[name])

    return best_state, best_acc, train_loss, train_acc, val_loss, val_acc


def custom_test(opt, test_dataloader, model):
    '''
    Test the model trained with the prototypical learning algorithm
    '''
    device = 'cuda:0' if torch.cuda.is_available() and opt.cuda else 'cpu'
    avg_acc = list()
    for epoch in range(10):
        test_iter = iter(test_dataloader)
        for batch in test_iter:
            x, y = batch
            x, y = x.to(device), y.to(device)
            model_output = model(x)
            print(model_output, y)
    #         _, acc = loss_fn(model_output, target=y,
    #                          n_support=opt.num_support_val)
    #         avg_acc.append(acc.item())
    # avg_acc = np.mean(avg_acc)
    # print('Test Acc: {}'.format(avg_acc))

    # return avg_acc


def eval(opt, options, test_df):
    '''
    Initialize everything and train
    '''
    # options = get_parser() #.parse_args()

    if torch.cuda.is_available() and not options.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")

    init_seed(options)
    test_dataloader = init_dataset(options, test_df, 'test' )[-1]
    model = init_protonet(options)
    model_path = os.path.join(opt.experiment_root, 'best_model.pth')
    model.load_state_dict(torch.load(model_path))

    custom_test(opt=options,
         test_dataloader=test_dataloader,
         model=model)
def main(options, df, test_df):
    '''
    Initialize everything and train
    '''
    # options = get_parser() #.parse_args()
    if not os.path.exists(options.exp):
        os.makedirs(options.exp)

    if torch.cuda.is_available() and not options.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")

    init_seed(options)

    tr_dataloader = init_dataloader(options, df, 'train')
    val_dataloader = init_dataloader(options, df, 'val')
    # trainval_dataloader = init_dataloader(options, 'trainval')
    test_dataloader = init_dataloader(options, test_df, 'test')

    model = init_protonet(options)
    optim = init_optim(options, model)
    lr_scheduler = init_lr_scheduler(options, optim)
    res = train(opt=options,
                tr_dataloader=tr_dataloader,
                val_dataloader=val_dataloader,
                model=model,
                optim=optim,
                lr_scheduler=lr_scheduler)
    best_state, best_acc, train_loss, train_acc, val_loss, val_acc = res
    print('Testing with last model..')
    custom_test(opt=options,
         test_dataloader=test_dataloader,
         model=model)

    model.load_state_dict(best_state)
    print('Testing with best model..')
    custom_test(opt=options,
         test_dataloader=test_dataloader,
         model=model)
#%%
# coding=utf-8
# import os
# import argparse

# class Args:
#   root='..' + os.sep + 'datasets/omniglot/python/images_background'
#   dataset_root=root
#   exp='..' + os.sep + 'output'
#   experiment_root=exp
#   nep=100
#   epochs=nep
#   lr=0.001
#   learning_rate=lr
#   lrS=20
#   lr_scheduler_step=lrS
#   lrG=0.5
#   lr_scheduler_gamma=lrG
#   its=100
#   iterations=its
#   cTr=60
#   classes_per_it_tr=cTr
#   nsTr=5
#   num_support_tr=nsTr
#   nqTr=5
#   num_query_tr=nqTr
#   cVa=5
#   classes_per_it_val=cVa
#   seed=5
#   nqVa=15
#   num_query_val=nqVa
#   nsVa=5
#   num_support_val=nsVa
#   cuda=True
#   manual_seed=seed

# def get_parser():
#     # parser = argparse.ArgumentParser()
#     # parser.add_argument('-root', '--dataset_root',
#     #                     type=str,
#     #                     help='path to dataset',
#     #                     default='..' + os.sep + 'dataset')

#     # parser.add_argument('-exp', '--experiment_root',
#     #                     type=str,
#     #                     help='root where to store models, losses and accuracies',
#     #                     default='..' + os.sep + 'output')

#     # parser.add_argument('-nep', '--epochs',
#     #                     type=int,
#     #                     help='number of epochs to train for',
#     #                     default=100)

#     # parser.add_argument('-lr', '--learning_rate',
#     #                     type=float,
#     #                     help='learning rate for the model, default=0.001',
#     #                     default=0.001)

#     # parser.add_argument('-lrS', '--lr_scheduler_step',
#     #                     type=int,
#     #                     help='StepLR learning rate scheduler step, default=20',
#     #                     default=20)

#     # parser.add_argument('-lrG', '--lr_scheduler_gamma',
#     #                     type=float,
#     #                     help='StepLR learning rate scheduler gamma, default=0.5',
#     #                     default=0.5)

#     # parser.add_argument('-its', '--iterations',
#     #                     type=int,
#     #                     help='number of episodes per epoch, default=100',
#     #                     default=100)

#     # parser.add_argument('-cTr', '--classes_per_it_tr',
#     #                     type=int,
#     #                     help='number of random classes per episode for training, default=60',
#     #                     default=60)

#     # parser.add_argument('-nsTr', '--num_support_tr',
#     #                     type=int,
#     #                     help='number of samples per class to use as support for training, default=5',
#     #                     default=5)

#     # parser.add_argument('-nqTr', '--num_query_tr',
#     #                     type=int,
#     #                     help='number of samples per class to use as query for training, default=5',
#     #                     default=5)

#     # parser.add_argument('-cVa', '--classes_per_it_val',
#     #                     type=int,
#     #                     help='number of random classes per episode for validation, default=5',
#     #                     default=5)

#     # parser.add_argument('-nsVa', '--num_support_val',
#     #                     type=int,
#     #                     help='number of samples per class to use as support for validation, default=5',
#     #                     default=5)

#     # parser.add_argument('-nqVa', '--num_query_val',
#     #                     type=int,
#     #                     help='number of samples per class to use as query for validation, default=15',
#     #                     default=15)

#     # parser.add_argument('-seed', '--manual_seed',
#     #                     type=int,
#     #                     help='input for the manual seeds initializations',
#     #                     default=7)

#     # parser.add_argument('--cuda',
#     #                     action='store_true',
#     #                     help='enables cuda')

#     # return parser
#     return Args()
# %%
