
from torch.utils import data
from torchvision import transforms as tfs
from PIL import Image as pil_image
import torch.nn as nn
import numpy as np
import random

NORMALIZATION_MEAN = [0.485, 0.456, 0.406]
NORMALIZATION_STD = [0.229, 0.224, 0.225]
IMAGE_SIZE=(224, 224)

## Cache training images to speedup train dataloader
cache = {}
# resize = tfs.Resize(IMAGE_SIZE)

def get_image(image_name):
    '''Returns cropped and resized image either from cache or from disk'''
    return cache.get(image_name) or load_image(image_name)


class CattleDataset(data.Dataset):

    def __init__(self, image_data, mode='train', transform=None):

        if transform:
            self.transform = transform
        else:
            print("No transforms given, Default transforms will be used.")
            self.transform = tfs.Compose([
                tfs.Resize(size=(512,512)),
                tfs.CenterCrop(size=(256,256)),
                tfs.ToTensor(),
                tfs.Normalize(NORMALIZATION_MEAN, NORMALIZATION_STD)
            ])

        # print('Images: {}. Augmentation: {}. Scope: {}.'.format(len(self.image_data), transform, scope))

        if mode=='train' or mode=='validation' or mode=='val':
            grouped = image_data.groupby('class')
            validation_indexes = []
            for group in grouped.groups.items():
                indexes = group[1]
                # Take only one image from class which has at least 3 images
                if (len(indexes) > 2):
                    validation_indexes.append(indexes[0])
            if mode == "validation" or mode=="val":
                self.image_data = image_data.iloc[validation_indexes].reset_index(drop=True)
            else:
                self.image_data = image_data[~image_data.index.isin(validation_indexes)].reset_index(drop=True)
        else:
            self.image_data = image_data
        self.y = list(self.image_data['class'].values)

    def __getitem__(self, idx):

        '''
        For train and validation triplets are required, for prediction - only images;
        '''
        row = self.image_data.iloc[idx]
        anchor_name = row['path']
        anchor = get_image(anchor_name)
        anchor = self.transform(anchor)
        return anchor, str(row['class'])

    def __len__(self):
        return len(self.image_data)
    

def load_image(image_name):
    image = pil_image.open(image_name).convert('RGB')
        # image = image.rotate(180)
    # return tfs.Resize(IMAGE_SIZE)(image)
    return image

# data = data.transform(get_transforms(do_flip=True, flip_vert=True), size=224)


"""
## Some random transforms ##

tfs1 = tfs.Compose([
                # tfs.ColorJitter(brightness=0, contrast=0.05, saturation=0.05),
                # tfs.RandomHorizontalFlip(p=0.5),
                tfs.ToTensor(),
                tfs.Normalize(NORMALIZATION_MEAN, NORMALIZATION_STD)
          ])

tfs2 = tfs.Compose([
    # tfs.Pad(25, padding_mode='symmetric'),
    tfs.Resize(size=(512,512)),
    # tfs.CenterCrop(size=IMAGE_SIZE),
    tfs.CenterCrop(size=(400,400)),
    tfs.CenterCrop(size=IMAGE_SIZE),
    tfs.RandomApply(nn.ModuleList([
       tfs.ColorJitter(brightness=.5, hue=.3),
    ]), p=0.1),
    tfs.RandomHorizontalFlip(p=0.2),
    tfs.RandomPerspective(distortion_scale=0.3, p=0.15),
    tfs.RandomGrayscale(p=0.15),
    tfs.RandomAutocontrast(p=0.2),
    tfs.RandomApply(nn.ModuleList([
      tfs.RandomRotation(10),
    ]), p=0.2),
    tfs.ToTensor(),
    tfs.Normalize(NORMALIZATION_MEAN, NORMALIZATION_STD),
    tfs.RandomErasing(p=0.33,scale=(0.02, 0.1),value=1.0, inplace=False)
])

"""